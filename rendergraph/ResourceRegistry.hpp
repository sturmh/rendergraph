//
//  ResourceRegistry.hpp
//  rendergraph
//
//  Created by Eugene Sturm on 4/12/18.
//  Copyright © 2018 Eugene Sturm. All rights reserved.
//

#ifndef ResourceRegistry_hpp
#define ResourceRegistry_hpp

#include <map>
#include "AttachmentDesc.h"

class RenderGraphResource;

class ResourceRegistry
{
public:
    RenderGraphResource* getTextureResource(const std::string& name) const;
    RenderGraphResource* createTextureResource(const std::string& name, const AttachmentDesc& desc);
private:
    std::map<std::string, std::unique_ptr<RenderGraphResource>> _resources;
};




#endif /* ResourceRegistry_hpp */
