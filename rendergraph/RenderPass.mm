//
//  RenderPass.cpp
//  rendergraph
//
//  Created by Eugene Sturm on 4/12/18.
//  Copyright © 2018 Eugene Sturm. All rights reserved.
//

#include "RenderPass.hpp"
#include "ResourceRegistry.hpp"
#include "RenderGraphResource.hpp"

RenderPass::RenderPass(RenderGraphExecutionDelegate executionDelegate,
           std::vector<const RenderGraphResource*>&& colorOutputs,
           const RenderGraphResource* depthStencilOutput,
           std::vector<const RenderGraphResource*>&& attachmentInputs,
           const RenderGraphResource* depthStencilInput,
           const std::string& name)
: _name(name)
, _delegate(executionDelegate)
, _colorOutputs(colorOutputs)
, _depthStencilOutput(depthStencilOutput)
, _attachmentInputs(attachmentInputs)
, _depthStencilInput(depthStencilInput)
{
}

const std::string& RenderPass::name() const
{
    return _name;
}

void RenderPass::execute(const std::map<std::string, id<MTLTexture>>& inputs, id<MTLRenderCommandEncoder> encoder) const {
    if (_delegate) {
        _delegate(encoder, inputs);
    }
}

const RenderGraphResource* RenderPass::depthStencilOutput() const { return _depthStencilOutput; }
const RenderGraphResource* RenderPass::depthStencilInput() const { return _depthStencilInput; }
const std::vector<const RenderGraphResource*>& RenderPass::colorOutputs() const { return _colorOutputs; }
const std::vector<const RenderGraphResource*>& RenderPass::attachmentInputs() const { return _attachmentInputs; }
