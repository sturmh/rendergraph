//
//  RenderGraph.cpp
//  rendergraph
//
//  Created by Eugene Sturm on 4/12/18.
//  Copyright © 2018 Eugene Sturm. All rights reserved.
//

#include "RenderGraph.hpp"
#include "ResourceRegistry.hpp"
#include "RenderGraphResource.hpp"
#include "RenderPass.hpp"
#include "RenderPassBuilder.hpp"
#include <set>
#include <queue>



RenderGraph::RenderGraph()
{
    _registry.reset(new ResourceRegistry());
}

RenderGraph::~RenderGraph()
{
    
}

RenderPass* RenderGraph::addRenderPass(const std::string& name, RenderGraphSetupDelegate setup, RenderGraphExecutionDelegate execute)
{
    RenderPassBuilder builder = RenderPassBuilder(_registry.get(), name, execute);
    setup(&builder);
    RenderPass* renderPass = builder.build();
    _passes.emplace_back(renderPass);
    return renderPass;
}

void RenderGraph::setBackbuffer(const std::string& name, const AttachmentDesc& desc)
{
    _backbuffer = name;
    _backbufferDesc = desc;
}

void RenderGraph::validate()
{
    // TODO
}

void RenderGraph::execute(id<MTLDevice> device, id<MTLCommandBuffer> commandBuffer, id<MTLTexture> backbuffer)
{
    std::vector<const RenderPass*> order = bake();
    
    std::set<const RenderGraphResource*> resources;
    
    for (const RenderPass* pass : order) {
        for (const RenderGraphResource* resource : pass->attachmentInputs()) {
            resources.insert(resource);
        }
        
        for (const RenderGraphResource* resource : pass->colorOutputs()) {
            resources.insert(resource);
        }
        
        if (pass->depthStencilInput()) {
            resources.insert(pass->depthStencilInput());
        }
        
        if (pass->depthStencilOutput()) {
            resources.insert(pass->depthStencilOutput());
        }
    }
    
    for (const RenderGraphResource* resource : resources) {
        auto it = _resourceTextureMap.find(resource);
        if (it != end(_resourceTextureMap)) {
            continue;
        }
        
        MTLTextureUsage textureUsage = 0;
        textureUsage |= resource->readPasses().size() > 0 ? MTLTextureUsageShaderRead : 0;
        textureUsage |= resource->writePasses().size() > 0 ? MTLTextureUsageRenderTarget : 0;
        
        const AttachmentDesc& desc = resource->desc();
        
        uint32_t width = 0;
        uint32_t height = 0;
        switch(desc.sizeType) {
            case SizeType::SwapchainRelative:
            {
                width = static_cast<uint32_t>(desc.width * _backbufferDesc.width);
                height = static_cast<uint32_t>(desc.height * _backbufferDesc.height);
                break;
            }
            case SizeType::Absolute:
            {
                width = static_cast<uint32_t>(desc.width);
                height = static_cast<uint32_t>(desc.height);
                break;
                
            }
        }
        
        MTLTextureDescriptor* mtlDesc = [[MTLTextureDescriptor alloc] init];
        mtlDesc.textureType           = MTLTextureType2D;
        mtlDesc.width                 = width;
        mtlDesc.height                = height;
        mtlDesc.pixelFormat           = Adapter::toMTL(desc.format);
        mtlDesc.depth                 = 1;
        mtlDesc.mipmapLevelCount      = 1;
        mtlDesc.sampleCount           = 1;
        mtlDesc.arrayLength           = 1;
        mtlDesc.cpuCacheMode          = MTLCPUCacheModeDefaultCache;
        mtlDesc.storageMode           = MTLStorageModePrivate;
        mtlDesc.usage                 = textureUsage;
        
        _resourceTextureMap[resource] = [device newTextureWithDescriptor:mtlDesc];
        _resourceTextureMap[resource].label = [NSString stringWithUTF8String:resource->name().c_str()];
    }
    
    std::vector<MTLRenderPassDescriptor*> passDescs;
    std::map<std::string, id<MTLTexture>> textures;
    for (const RenderPass* renderPass : order) {
        MTLRenderPassDescriptor* renderPassDesc = [MTLRenderPassDescriptor renderPassDescriptor];
        int index = 0;
        
        for (const RenderGraphResource* colorOutput : renderPass->colorOutputs()) {
            id<MTLTexture> texture = colorOutput->name() == _backbuffer ? backbuffer : (_resourceTextureMap.find(colorOutput) != end(_resourceTextureMap) ? _resourceTextureMap[colorOutput] : nullptr);
            textures[colorOutput->name()] = texture;
            if (texture) {
                const AttachmentDesc& attachmentDesc = colorOutput->desc();
                MTLRenderPassColorAttachmentDescriptor* colorAttachment = [MTLRenderPassColorAttachmentDescriptor new];
                colorAttachment.texture = texture;
                colorAttachment.loadAction = attachmentDesc.loadAction;
                colorAttachment.storeAction = attachmentDesc.storeAction;
                colorAttachment.clearColor = attachmentDesc.clearColor;
                
                [renderPassDesc.colorAttachments setObject:colorAttachment atIndexedSubscript:index++];
            }
        }
        
        if (renderPass->depthStencilOutput()) {
            id<MTLTexture> depthStencilTexture = _resourceTextureMap.find(renderPass->depthStencilOutput()) != end(_resourceTextureMap) ? _resourceTextureMap[renderPass->depthStencilOutput()] : nullptr;
            
            const AttachmentDesc& attachmentDesc = renderPass->depthStencilOutput()->desc();
            textures[renderPass->depthStencilOutput()->name()] = depthStencilTexture;
            if (depthStencilTexture && isDepthFormat(depthStencilTexture.pixelFormat)) {                
                MTLRenderPassDepthAttachmentDescriptor* depthAttachment = [MTLRenderPassDepthAttachmentDescriptor new];
                depthAttachment.texture = depthStencilTexture;
                depthAttachment.loadAction = attachmentDesc.loadAction;
                depthAttachment.storeAction = attachmentDesc.storeAction;
                depthAttachment.clearDepth = attachmentDesc.clearDepth;
                
                renderPassDesc.depthAttachment = depthAttachment;
            }
            
            if (depthStencilTexture && isStencilFormat(depthStencilTexture.pixelFormat)) {
                MTLRenderPassStencilAttachmentDescriptor* stencilAttachment = [MTLRenderPassStencilAttachmentDescriptor new];
                stencilAttachment.texture = depthStencilTexture;
                stencilAttachment.loadAction = attachmentDesc.loadAction;
                stencilAttachment.storeAction = attachmentDesc.storeAction;
                stencilAttachment.clearStencil = attachmentDesc.clearStencil;
                
                renderPassDesc.stencilAttachment = stencilAttachment;
            }
        }
        
        passDescs.push_back(renderPassDesc);
    }
    
    int idx = 0;
    for (const RenderPass* renderPass : order) {        
        MTLRenderPassDescriptor* renderPassDesc = passDescs[idx++];
        [commandBuffer pushDebugGroup:[NSString stringWithUTF8String:renderPass->name().c_str()]];
        id<MTLRenderCommandEncoder> encoder = [commandBuffer renderCommandEncoderWithDescriptor:renderPassDesc];
        
        encoder.label = [NSString stringWithUTF8String:renderPass->name().c_str()];
        
        renderPass->execute(textures, encoder);
        
        [encoder endEncoding];
        [commandBuffer popDebugGroup];
        
    }
    
}


std::vector<const RenderPass*> RenderGraph::bake()
{
    validate();
    
    std::set<const RenderPass*> existing;
    std::vector<const RenderPass*> order;
    
    RenderGraphResource* backbufferResource = _registry->getTextureResource(_backbuffer);
    if (backbufferResource == nullptr) {
        // problem
        return order;
    }
    
    std::map<const RenderGraphResource* , std::set<RenderPass*>> writePasses;
    std::map<const RenderGraphResource* , std::set<RenderPass*>> readPasses;
    for (const std::unique_ptr<RenderPass>& passPtr : _passes) {
        RenderPass* pass = passPtr.get();
        if (pass->depthStencilOutput()) {
            writePasses[pass->depthStencilOutput()].insert(pass);
        }
        
        if (pass->depthStencilInput()) {
            readPasses[pass->depthStencilInput()].insert(pass);
        }
        
        for (auto resource : pass->colorOutputs()) {
            writePasses[resource].insert(pass);
        }
        
        for (auto resource : pass->attachmentInputs()) {
            readPasses[resource].insert(pass);
        }
    }
    
    std::queue<const RenderGraphResource*> q;
    q.push(backbufferResource);
    
    while (q.empty() == false) {
        const RenderGraphResource* resource = q.front();
        q.pop();
        
        for (const RenderPass* pass : writePasses[resource]) {
            if (existing.count(pass) == 0) {
                order.emplace_back(pass);
                existing.insert(pass);
                
                if (pass->depthStencilInput()) {
                    q.push(pass->depthStencilInput());
                }
                
                for (const RenderGraphResource* inputResource : pass->attachmentInputs()) {
                    q.push(inputResource);
                }
            }
        }
    }
    
    std::reverse(begin(order), end(order));
    
    return order;
}
