//
//  RenderPassBuilder.hpp
//  rendergraph
//
//  Created by Eugene Sturm on 4/12/18.
//  Copyright © 2018 Eugene Sturm. All rights reserved.
//

#ifndef RenderPassBuilder_hpp
#define RenderPassBuilder_hpp

#include <string>
#include <functional>
#import <vector>
#include "AttachmentDesc.h"
#import <Metal/Metal.h>
#include <map>

class ResourceRegistry;
class RenderPass;
class RenderGraphResource;

using RenderGraphExecutionDelegate = std::function<void(id<MTLRenderCommandEncoder>, const std::map<std::string, id<MTLTexture>>&)>;

class RenderPassBuilder
{
private:
    ResourceRegistry* _registry;
    const RenderGraphResource* _depthStencilOutput { nullptr };
    const RenderGraphResource* _depthStencilInput { nullptr };
    std::vector<const RenderGraphResource*> _colorOutputs;
    std::vector<const RenderGraphResource*> _attachmentInputs;
    RenderGraphExecutionDelegate _delegate;
    std::string _name;
public:
    RenderPassBuilder(ResourceRegistry* registry, const std::string& name, RenderGraphExecutionDelegate delegate);
    
    void addColorOutput(const std::string& name, const AttachmentDesc& desc);
    void addAttachmentInput(const std::string& name);
    void setDepthStencilInput(const std::string& name);
    void setDepthStencilOutput(const std::string& name, const AttachmentDesc& desc);
    RenderPass* build();
};

#endif /* RenderPassBuilder_hpp */
