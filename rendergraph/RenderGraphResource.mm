//
//  RenderGraphResource.cpp
//  rendergraph
//
//  Created by Eugene Sturm on 4/12/18.
//  Copyright © 2018 Eugene Sturm. All rights reserved.
//

#include "RenderGraphResource.hpp"
#include "RenderPass.hpp"

RenderGraphResource::RenderGraphResource(RenderGraphResourceType type, const AttachmentDesc& desc, const std::string& name) : _type(type), _desc(desc), _name(name) {}

void RenderGraphResource::addWritePass(const RenderPass* pass)
{
    _writePasses.insert(pass);
}

void RenderGraphResource::removeWritePass(const RenderPass* pass)
{
    _writePasses.erase(pass);
}

void RenderGraphResource::addReadPass(const RenderPass* pass)
{
    _readPasses.insert(pass);
}

void RenderGraphResource::removeReadPass(const RenderPass* pass)
{
    _readPasses.erase(pass);
}

const std::set<const RenderPass*>& RenderGraphResource::readPasses() const { return _readPasses; }
const std::set<const RenderPass*>& RenderGraphResource::writePasses() const { return _writePasses; }

RenderGraphResourceType RenderGraphResource::type() const
{
    return _type;
}

const std::string& RenderGraphResource::name() const
{
    return _name;
}

const AttachmentDesc& RenderGraphResource::desc() const
{
    return _desc;
}
