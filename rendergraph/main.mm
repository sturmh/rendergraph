//
//  main.cpp
//  rendergraph
//
//  Created by Eugene Sturm on 4/3/18.
//  Copyright © 2018 Eugene Sturm. All rights reserved.
//

#include <iostream>
#import <Metal/Metal.h>
#import <Foundation/Foundation.h>
#import <map>
#import <vector>
#import <set>
#import <queue>
#import <algorithm>
#import <QuartzCore/CAMetalLayer.h>
#import <SDL2/SDL.h>
#import <SDL2/SDL_syswm.h>
#import <AppKit/AppKit.h>
#import <array>
#include "RenderPassBuilder.hpp"
#include "RenderPass.hpp"
#include <glm/glm/glm.hpp>
#include <glm/glm/gtc/matrix_transform.hpp>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "RenderGraphResource.hpp"
#include "RenderGraph.hpp"


//class StateGroup
//{
//
//};
//
//
//template <typename T>
//using optional = std::pair<T, bool>;
//
struct FrameBuffer
{
    std::array<std::pair<AttachmentDesc, id<MTLTexture>>, 4> _attachments;
    std::pair<AttachmentDesc, id<MTLTexture>> _depthAttachment;
    std::pair<AttachmentDesc, id<MTLTexture>> _steniclAttachment;
};

//
//struct RenderPassInfo
//{
//    std::array<id<MTLTexture>, 4> colorAttachments;
//    id<MTLTexture> depthAttachment;
//    id<MTLTexture> stencilAttachment;
//
//    std::map<id<MTLTexture>, AttachmentDesc> descs;
//};
//



class RenderCommandEncoder
{
private:
    id<MTLRenderCommandEncoder> _encoder;
    id<MTLDevice> _device;
    MTLRenderPipelineDescriptor* _pipelineDesc;
public:
    RenderCommandEncoder(id<MTLRenderCommandEncoder> encoder, id<MTLDevice> device);
    
    void setVertexStageTexture(id<MTLTexture> texture, uint8_t index)
    {
        [_encoder setVertexTexture:texture atIndex:index];
    }
    void setVertexStageBuffer(id<MTLBuffer> buffer, uint8_t index)
    {
        [_encoder setVertexBuffer:buffer offset:0 atIndex:index];
    }
    
    void setFragmentStageTexture(id<MTLTexture> texture, uint8_t index)
    {
        [_encoder setFragmentTexture:texture atIndex:index];
    }
    
    void setFragmentStageBuffer(id<MTLBuffer> buffer, uint8_t index)
    {
        [_encoder setFragmentBuffer:buffer offset:0 atIndex:index];
    }

    void setVertexLayout(MTLVertexDescriptor* inputLayout)
    {
        
    }

    void setVertexBuffer(id<MTLBuffer> buffer, uint8_t index)
    {
        [_encoder setVertexBuffer:buffer offset:0 atIndex:index];
    }

    void setVertexShader(id<MTLFunction> vertexShader)
    {
        
    }
    
    void setFragmentShader(id<MTLFunction> fragmentShader)
    {
        
    }
    
    void setDepthStencilState(id<MTLDepthStencilState> depthStencilState)
    {
        [_encoder setDepthStencilState:depthStencilState];
    }
    
    void setCullMode(MTLCullMode cullMode)
    {
        
    }
    void setFrontFacingWinding(MTLWinding winding);
    void setViewport(MTLViewport viewport);
    void setTriangleFillMode(MTLTriangleFillMode fillMode);
    
    void drawIndexed(id<MTLBuffer> indexBuffer, uint32_t primitiveCount, uint32_t startOffset, uint32_t baseVertexOffset);
};


class CommandBuffer
{
public:
    RenderCommandEncoder* beginRenderPass(FrameBuffer* frameBuffer);
    void endRenderPass(RenderCommandEncoder* encoder);
};

struct ResourceBindings
{
    std::vector<std::pair<id<MTLBuffer>, uint8_t>> buffers;
    std::vector<std::pair<id<MTLBuffer>, uint8_t>> textures;
    id<MTLBuffer> vertexBuffer;
    id<MTLBuffer> indexBuffer;
};

class RenderPassDesc
{
    
};

struct RasterState {};
struct BlendState {};
struct DepthStencilState {};

enum class DrawCallType
{
    Indexed,
    Primitives,
};


struct DrawCall
{
    uint32_t primitiveCount;
    uint32_t startOffset;
    uint32_t baseVertexOffset;
    DrawCallType type;
};



enum class StateGroupIndex : uint16_t {
    VertexShader  = 0,
    PixelShader   = 1,
    BlendState    = 2,
    RasterState   = 3,
    DepthState    = 4,
    IndexBuffer   = 5,
    VertexBuffer  = 6,
    VertexLayout  = 7,
    PrimitiveType = 8,
    Bindings      = 9,
};

enum class StateGroupBit : uint16_t {
    VertexShader  = 1 << static_cast<uint16_t>(StateGroupIndex::VertexShader),
    PixelShader   = 1 << static_cast<uint16_t>(StateGroupIndex::PixelShader),
    BlendState    = 1 << static_cast<uint16_t>(StateGroupIndex::BlendState),
    RasterState   = 1 << static_cast<uint16_t>(StateGroupIndex::RasterState),
    DepthState    = 1 << static_cast<uint16_t>(StateGroupIndex::DepthState),
    IndexBuffer   = 1 << static_cast<uint16_t>(StateGroupIndex::IndexBuffer),
    VertexBuffer  = 1 << static_cast<uint16_t>(StateGroupIndex::VertexBuffer),
    VertexLayout  = 1 << static_cast<uint16_t>(StateGroupIndex::VertexLayout),
    PrimitiveType = 1 << static_cast<uint16_t>(StateGroupIndex::PrimitiveType),
    Bindings      = 1 << static_cast<uint16_t>(StateGroupIndex::Bindings),
};

// if it stateless, would have to reset state to defaults between each draw call
// could just make a "deferred command buffer" and an immediate command buffer

class StateGroup
{
public:
    void setRenderPass() {
        
    }
    void setBlendState() {
        
    }
    void setDepthSteniclState() {
        
    }
    void setRasterState() {
        
    }
    void setVertexFunction();
    void setFragmentFunction();
    void setInputLayout();
    void setViewport();
    void setCullMode();
    void setWinding();
private:
    std::vector<uint8_t> _payload;
};


class DrawItem {
public:
}

class PipelineState
{
    
};



//void compilePipelineState(const std::stack<StateGroup*> stageGroupStack)
//{
//
//};


struct GraphicsPipelineSetup
{
    std::vector<std::pair<id<MTLBuffer>, uint8_t>> buffers;
    std::vector<std::pair<id<MTLBuffer>, uint8_t>> textures;
    id<MTLBuffer> vertexBuffer;
    id<MTLBuffer> indexBuffer;
};

class DrawItem;




struct DrawItem
{
    id<MTLRenderPipelineState> pipelineState;
    
    struct {
        struct {
            std::array<std::pair<id<MTLBuffer>, uint8_t>, 8> buffers;
            uint8_t bufferCount { 0 };
            std::array<std::pair<id<MTLTexture>, uint8_t>, 8> textures;
            uint8_t textureCount { 0 };
        } vertex;

        struct {
            std::array<std::pair<id<MTLBuffer>, uint8_t>, 8> buffers;
            uint8_t bufferCount { 0 };
            std::array<std::pair<id<MTLTexture>, uint8_t>, 8> textures;
            uint8_t textureCount { 0 };
        } fragment;
    } resourceBindings;
    
    id<MTLBuffer> vertexBuffer;
    id<MTLBuffer> indexBuffer;
    
    DrawCall drawCall;
};

using RenderPassId = size_t;


class RenderPassCommandEncoder
{
private:
    id<MTLDevice> _device;
    MTLRenderPipelineDescriptor* _pipelineDesc;
    DrawItem* _item { nullptr };
    id<MTLRenderCommandEncoder> _encoder;
public:
    RenderPassCommandEncoder(id<MTLDevice> device, RenderPass* renderPass)
    : _device(device)
    , _pipelineDesc([[MTLRenderPipelineDescriptor alloc] init])
    , _item(new DrawItem())
    {
        
        int idx = 0;
        for (const RenderGraphResource* resource : renderPass->colorOutputs()) {
            _pipelineDesc.colorAttachments[idx++].pixelFormat = Adapter::toMTL(resource->desc().format);
        }
        
        const RenderGraphResource* depthStencilResource = renderPass->depthStencilOutput();
        if (depthStencilResource) {
            MTLPixelFormat format = Adapter::toMTL(depthStencilResource->desc().format);
            if (isStencilFormat(format)) {
                _pipelineDesc.stencilAttachmentPixelFormat = format;
            }
            
            if (isDepthFormat(format)) {
                _pipelineDesc.depthAttachmentPixelFormat = format;
            }
        }
    }
    
    ~RenderPassCommandEncoder()
    {
        delete _item;
    }
    
    void setDepthStencilState(id<MTLDepthStencilState> depthStencilState)
    {
        
    }
    
    void setCullMode(MTLCullMode cullMode)
    {
        
    }
    
    void setFrontFacingWinding(MTLWinding winding)
    {
        
    }
    
    void setViewport(MTLViewport viewport)
    {
        
    }
    
    void setTriangleFillMode(MTLTriangleFillMode fillMode)
    {
        
    }
    
    void setVertexStageTexture(id<MTLTexture> texture, uint8_t index)
    {
        _item->resourceBindings.vertex.textures[_item->resourceBindings.vertex.textureCount++] = { texture, index };
    }
    void setVertexStageBuffer(id<MTLBuffer> buffer, uint8_t index)
    {
        _item->resourceBindings.vertex.buffers[_item->resourceBindings.vertex.bufferCount++] = { buffer, index };
    }
    void setFragmentStageTexture(id<MTLTexture> texture, uint32_t index)
    {
        _item->resourceBindings.fragment.textures[_item->resourceBindings.fragment.textureCount++] = { texture, index };
    }
    void setFragmentStageBuffer(id<MTLBuffer> buffer, uint32_t index)
    {
        _item->resourceBindings.fragment.buffers[_item->resourceBindings.fragment.bufferCount++] = { buffer, index };
    }
    void setInputLayout(MTLVertexDescriptor* inputLayout)
    {
        _pipelineDesc.vertexDescriptor = inputLayout;
    }
    void setIndexBuffer(id<MTLBuffer> indexBuffer)
    {
        _item->indexBuffer = indexBuffer;
    }
    void setVertexBuffer(id<MTLBuffer> vertexBuffer)
    {
        _item->vertexBuffer = vertexBuffer;
    }
    void setVertexShader(id<MTLFunction> vertexShader)
    {
        _pipelineDesc.vertexFunction = vertexShader;
    }
    void setFragmentShader(id<MTLFunction> fragmentShader)
    {
        _pipelineDesc.fragmentFunction = fragmentShader;
    }
    
    DrawItem* drawIndexed(uint32_t primitiveCount, uint32_t startOffset, uint32_t baseVertexOffset)
    {
        DrawCall drawCall;
        drawCall.primitiveCount = primitiveCount;
        drawCall.startOffset = startOffset;
        drawCall.baseVertexOffset = baseVertexOffset;
        drawCall.type = DrawCallType::Indexed;
        
        return finish(std::move(drawCall));
    }
    
    DrawItem* drawPrimitives(uint32_t primitiveCount, uint32_t startOffset)
    {
        DrawCall drawCall;
        drawCall.primitiveCount = primitiveCount;
        drawCall.startOffset = startOffset;
        drawCall.type = DrawCallType::Primitives;
        
        return finish(std::move(drawCall));
    }
private:
    DrawItem* finish(DrawCall&& drawCall)
    {
        _item->pipelineState = getPipelineState();
        _item->drawCall = std::move(drawCall);
        
        DrawItem* item = new DrawItem();
        std::swap(item, _item);
        return item;
    }
    
    id<MTLRenderPipelineState> getPipelineState()
    {
        NSError* error = nil;
        MTLRenderPipelineReflection* reflection = nil;
        MTLPipelineOption pipelineOptions = MTLPipelineOptionBufferTypeInfo | MTLPipelineOptionArgumentInfo;
        return [_device newRenderPipelineStateWithDescriptor:_pipelineDesc options:pipelineOptions reflection:&reflection error:&error];
    }
    
};

class RenderItemEncoder
{
public:
    
};

//
//class CommandBuffer
//{
//public:
//    virtual void beginRenderPass(const RenderPassInfo& passInfo) = 0;
//    virtual void endRenderPass() = 0;
//
//    virtual void setVertexStageTexture(id<MTLTexture> texture, uint32_t index) = 0;
//    virtual void setVertexStageBuffer(id<MTLBuffer> buffer, uint32_t index) = 0;
//    virtual void setFragmentStageTexture(id<MTLTexture> texture, uint32_t index) = 0;
//    virtual void setFragmentStageBuffer(id<MTLBuffer> buffer, uint32_t index) = 0;
//
//    virtual void setInputLayout(MTLVertexDescriptor* inputLayer) = 0;
//    virtual void setIndexBuffer(id<MTLBuffer> indexBuffer) = 0;
//    virtual void setVertexBuffer(id<MTLBuffer> vertexBuffer) = 0;
//
//    virtual void setVertexShader(id<MTLFunction> vertexShader) = 0;
//    virtual void setFragmentShader(id<MTLFunction> fragmentShader) = 0;
//
//    virtual void drawIndexed(uint32_t primitiveCount, uint32_t startOffset, uint32_t baseVertexOffset) = 0;
//};
//
//class MetalCommandBuffer : public CommandBuffer
//{
//private:
//    id<MTLCommandBuffer> _commandBuffer;
//    id<MTLDevice> _device;
//
//    struct {
//        struct {
//            std::array<id<MTLBuffer>, 16> buffers;
//            std::array<id<MTLTexture>, 16> textures;
//        } vertex;
//
//        struct {
//            std::array<id<MTLBuffer>, 16> buffers;
//            std::array<id<MTLTexture>, 16> textures;
//        } fragment;
//    } _resourceBindings;
//
//    MTLVertexDescriptor* _inputLayout;
//    id<MTLBuffer> _indexBuffer;
//    id<MTLFunction> _vertexFunction;
//    id<MTLFunction> _fragmentFunction;
//    RenderPassInfo _renderPassInfo;
//
//    std::map<RenderPassInfo, id<MTLRenderCommandEncoder>> _encoders;
//    
//    id<MTLRenderCommandEncoder> _currentEncoder;
//
//public:
//    MetalCommandBuffer(id<MTLCommandBuffer> commandBuffer, id<MTLDevice> device)
//    {
//        _commandBuffer = commandBuffer;
//        _device = device;
//    }
//
//    void beginRenderPass(const RenderPassInfo& passInfo) final
//    {
//        _encoders.find(passInfo);
//    }
//
//    virtual void endRenderPass() = 0;
//
//    virtual void setVertexStageTexture(id<MTLTexture> texture, uint32_t index) = 0;
//    virtual void setVertexStageBuffer(id<MTLBuffer> buffer, uint32_t index) = 0;
//    virtual void setFragmentStageTexture(id<MTLTexture> texture, uint32_t index) = 0;
//    virtual void setFragmentStageBuffer(id<MTLBuffer> buffer, uint32_t index) = 0;
//
//    virtual void setInputLayout(MTLVertexDescriptor* inputLayer) = 0;
//    virtual void setIndexBuffer(id<MTLBuffer> indexBuffer) = 0;
//    virtual void setVertexBuffer(id<MTLBuffer> vertexBuffer) = 0;
//
//    virtual void setVertexShader(id<MTLFunction> vertexShader) = 0;
//    virtual void setFragmentShader(id<MTLFunction> fragmentShader) = 0;
//
//    virtual void drawIndexed(uint32_t primitiveCount, uint32_t startOffset, uint32_t baseVertexOffset)
//        
//
////    void beginRenderPass(const RenderPassInfo& renderPassInfo)
////    {
////        _renderPassInfo = renderPassInfo;
////    }
////
////    void setVertexTexture(id<MTLTexture> texture, uint32_t index)
////    {
////        _resourceBindings.vertex.textures[index] = texture;
////    }
////    void setVertexBuffer(id<MTLBuffer> buffer, uint32_t index)
////    {
////        _resourceBindings.vertex.buffers[index] = buffer;
////    }
////    void setFragmentTexture(id<MTLTexture> texture, uint32_t index)
////    {
////        _resourceBindings.fragment.textures[index] = texture;
////    }
////    void setFragmentBuffer(id<MTLBuffer> buffer, uint32_t index)
////    {
////        _resourceBindings.fragment.buffers[index] = buffer;
////    }
////    void setInputLayout(MTLVertexDescriptor* inputLayer)
////    {
////        _inputLayout = inputLayer;
////    }
////    void setIndexBuffer(id<MTLBuffer> indexBuffer)
////    {
////        _indexBuffer = indexBuffer;
////    }
////    void setVertexShader(id<MTLFunction> vertexFunction)
////    {
////        _vertexFunction = vertexFunction;
////    }
////    void setPixelShader(id<MTLFunction> fragmentFunction)
////    {
////        _fragmentFunction = fragmentFunction;
////    }
////    void drawIndexed()
////    {
////
////    }
//private:
//    void createGraphicsPipeline()
//    {
//        MTLRenderPipelineDescriptor* rpd = [[MTLRenderPipelineDescriptor alloc] init];
//        rpd.vertexDescriptor = _inputLayout;
//        rpd.vertexFunction = _vertexFunction;
//        rpd.fragmentFunction = _fragmentFunction;
//        for (int idx = 0; idx < _renderPassInfo.colorAttachments.size(); ++idx) {
//            id<MTLTexture> texture = _renderPassInfo.colorAttachments[idx];
//            if (texture == nil) {
//                break;
//            }
//
//            rpd.colorAttachments[idx].pixelFormat = texture.pixelFormat;
//        }
//
//        if (_renderPassInfo.depthAttachment) {
//            rpd.depthAttachmentPixelFormat = _renderPassInfo.depthAttachment.pixelFormat;
//        }
//
//        if (_renderPassInfo.depthAttachment) {
//            rpd.stencilAttachmentPixelFormat = _renderPassInfo.stencilAttachment.pixelFormat;
//        }
//
//        NSError* error = nil;
//        id<MTLRenderPipelineState> pipelineState = [_device newRenderPipelineStateWithDescriptor:rpd error:&error];
//    }
//
//};
//
////class RenderItem;
////
////class RenderQueue2
////{
////private:
////    using RenderItemPair = std::pair<uint32_t, RenderItem*>;
////public:
////    void addRenderItem(RenderPass* renderPass, RenderItem* item, uint32_t sortKey = 0)
////    {
////        _itemsByPass[renderPass].emplace_back(sortKey, item);
////    }
////
////    void submitPass(RenderPass* pass, CommandBuffer* commandBuffer)
////    {
////        std::vector<RenderItemPair>& items = _itemsByPass[pass];
////        std::stable_sort(begin(items), end(items), [](const RenderItemPair& lhs, const RenderItemPair& rhs) { return lhs.first < rhs.first; });
////
////        for (const RenderItemPair& item : items)
////        {
////
////        }
////
////        items.clear();
////    }
////private:
////    std::map<RenderPass*, std::vector<RenderItemPair>> _itemsByPass;
////};
//
//class RenderQueue
//{
//private:
//    id<MTLCommandBuffer> _commandBuffer;
//    id<MTLDevice> _device;
//public:
//    RenderQueue(id<MTLDevice> device, id<MTLCommandBuffer> commandBuffer)
//    {
//        _commandBuffer = commandBuffer;
//        _device = device;
//    }
//
//    CommandBuffer* beginPass()
//    {
//        MTLRenderPassDescriptor* renderPassDescriptor = nil;
//        id<MTLRenderCommandEncoder> encoder = [_commandBuffer renderCommandEncoderWithDescriptor:renderPassDescriptor];
//        return new CommandBuffer(encoder, _device, renderPassDescriptor);
//    }
//
//    void endPass(CommandBuffer* commandBuffer)
//    {
//
//    }
//};










#include "RenderGraph.hpp"

// view subscribes to render stages
// view has set of supported render stages
// render objects subscribe to render stages

//class RenderView
//{
//
//};
//
//class RenderObj
//{
//}
//
//class RenderQueue
//{
//
//}
//
//struct PrepareContext
//{
//}
//
//struct SubmitContext
//{
//
//}
//
//class Renderer
//{
//public:
//    virtual void prepare(PrepareContext* prepareContext, RenderView* view, RenderPass* pass, const std::vector<RenderObj*>& visibleObjects) = 0;
//    virtual void submit(SubmitContext* submitContext, RenderView* view, RenderPass* pass, RenderQueue* queue, const std::vector<RenderObj*>& visibleObjects) = 0;
//
//};

@interface MetalView : NSView
@end

@implementation MetalView

+ (Class)layerClass {
    return [CAMetalLayer class];
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        self.wantsLayer = YES;
        self.layer = [CAMetalLayer layer];
    }
    
    return self;
}

@end



struct Vertex {
    float pos[3];
    float norm[3];
};

struct Vertex2 {
    float pos[3];
    float norm[3];
    float uv[2];
};

#include <iostream>
#include <fstream>

bool readFileContents(const std::string& fpath, std::string* output) {

    std::ifstream fin(fpath, std::ios::in | std::ios::binary);
    
    if (fin.fail()) {
        return false;
    }
    
    output->insert(end(*output), std::istreambuf_iterator<char>(fin), std::istreambuf_iterator<char>());
    
    return true;
}

id<MTLDevice> device;
id<MTLCommandQueue> commandQueue;

using VertexDelegate = std::function<void(const float*, const float*, const float*)>;

static void generateGrid(float centerX, float centerY, float centerZ, float sizeX, float sizeY, uint32_t resolutionX, uint32_t resolutionY, VertexDelegate delegate) {
    
    float half_sizeX = sizeX / 2.f;
    float half_sizeY = sizeY / 2.f;
    
    float dx = sizeX / static_cast<float>(resolutionX - 1);
    float dy = sizeY / static_cast<float>(resolutionY - 1);
    
    // start in topLeft
    auto generateVertex = [&](uint32_t i, uint32_t j) {
        float pos[3];
        float uv[2];
        float norm[3] = {0, 1, 0};
        pos[0] = centerX - half_sizeX + (i * dx);
        pos[1] = centerY - half_sizeY + (j * dy);
        pos[2] = centerZ;
        uv[0] = i * dx / sizeX;
        uv[1] = j * dy / sizeY;
        delegate(pos, norm, uv);
    };
    
    for (uint32_t j = 0; j < resolutionY - 1; ++j) {
        for (uint32_t i = 0; i < resolutionX - 1; ++i) {
            generateVertex(i, j);
            generateVertex(i + 1, j);
            generateVertex(i + 1, j + 1);
            generateVertex(i + 1, j + 1);
            generateVertex(i, j + 1);
            generateVertex(i, j);
        }
    }
}

static void generateCube(float originX, float originY, float originZ, float scale, bool flip, VertexDelegate delegate) {
    static float positions[6][6][3] = {
        {{-0.5, -0.5, 0.5}, {0.5, -0.5, 0.5}, {0.5, 0.5, 0.5}, {0.5, 0.5, 0.5}, {-0.5, 0.5, 0.5}, {-0.5, -0.5, 0.5}}, // front
        
        {{0.5, -0.5, -0.5}, {-0.5, -0.5, -0.5}, {-0.5, 0.5, -0.5}, {-0.5, 0.5, -0.5}, {0.5, 0.5, -0.5}, {0.5, -0.5, -0.5}}, // back
        
        {{-0.5, -0.5, -0.5}, {-0.5, -0.5, 0.5}, {-0.5, 0.5, 0.5}, {-0.5, 0.5, 0.5}, {-0.5, 0.5, -0.5}, {-0.5, -0.5, -0.5}}, // left
        
        {{0.5, -0.5, 0.5}, {0.5, -0.5, -0.5}, {0.5, 0.5, -0.5}, {0.5, 0.5, -0.5}, {0.5, 0.5, 0.5}, {0.5, -0.5, 0.5}}, // right
        
        {{-0.5, 0.5, 0.5}, {0.5, 0.5, 0.5}, {0.5, 0.5, -0.5}, {0.5, 0.5, -0.5}, {-0.5, 0.5, -0.5}, {-0.5, 0.5, 0.5}}, // top
        
        {{0.5, -0.5, 0.5}, {-0.5, -0.5, 0.5}, {-0.5, -0.5, -0.5}, {-0.5, -0.5, -0.5}, {0.5, -0.5, -0.5}, {0.5, -0.5, 0.5}} // bottom
    };
    
    static float texCoords[6][2] = {
        {0.f, 0.f }, //bl
        {1.f, 0.f }, //br
        {1.f, 1.f }, //tr
        {1.f, 1.f }, //tr
        {0.f, 1.f }, //tl
        {0.f, 0.f }  //bl
    };
    
    static float normals[6][3] = {
        {  0.f,  0.f, -1.f }, // front
        {  0.f,  0.f,  1.f }, // back
        { -1.f,  0.f,  0.f }, // left
        {  1.f,  0.f,  0.f }, // right
        {  0.f,  1.f,  0.f }, // top
        {  0.f, -1.f,  0.f }, // bottom
    };
    
    for (uint32_t f = 0; f < 6; ++f) {     // for each face
        for (uint32_t v = 0; v < 6; ++v) { // for each vertex
            float pos[3];
            pos[0] = scale * positions[f][flip ? 5 - v : v][0] + originX;
            pos[1] = scale * positions[f][flip ? 5 - v : v][1] + originY;
            pos[2] = scale * positions[f][flip ? 5 - v : v][2] + originZ;
            
            delegate(pos, normals[f], texCoords[v]);
        }
    }
}

struct ViewConstants
{
    glm::mat4 view;
    glm::mat4 proj;
};

struct MeshConstants
{
    glm::mat4 world;
};

struct MeshRenderObject
{
    
};

class LightRenderObject
{
    
};

class RenderScene
{
    
};

int main(int argc, const char * argv[])
{
    
    if (SDL_Init(SDL_INIT_TIMER | SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER | SDL_INIT_EVENTS) != 0) {
        return -1;
    }
    
    glm::vec3 eyePos = glm::vec3(0, 0, -20);
    glm::vec3 target = glm::vec3(0, 0, 0);
    glm::vec3 shadowEyePos = glm::vec3(0, 0, -10);
    glm::vec3 shadowTarget = glm::vec3(0, 0, 0);
    glm::mat4 proj = glm::perspective(45.0, 1024.0/768.0, 0.001, 10000.0);
    glm::mat4 view = glm::lookAt(eyePos, target, glm::vec3(0.f, 1.f, 0.f));
    glm::mat4 shadowView = glm::lookAt(shadowEyePos, shadowTarget, glm::vec3(0, 1, 0));
    
    glm::mat4 cubeWorld = glm::scale(glm::mat4(), glm::vec3(1, 1, 1));
    cubeWorld = glm::translate(glm::mat4(), glm::vec3(0, 0, 0)) * cubeWorld;
    glm::mat4 gridWorld = glm::mat4();//glm::rotate(glm::mat4(), 3.14f/2.f, glm::vec3(1, 0, 0));
    
//    glm::mat4 normalMatrix = glm::transpose(glm::inverse(view * world));
    
    
    SDL_Window* window = SDL_CreateWindow("Rawr", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1024, 768, SDL_WINDOW_RESIZABLE);
    SDL_SysWMinfo info;
    SDL_VERSION(&info.version);
    SDL_GetWindowWMInfo(window, &info);
    
    NSWindow* hndl = (NSWindow*)info.info.cocoa.window;
    MetalView* subView = [[MetalView alloc] initWithFrame:hndl.contentView.frame];
    [hndl.contentView addSubview:subView];
    
    CAMetalLayer* metalLayer = (CAMetalLayer*)subView.layer;
    device = MTLCreateSystemDefaultDevice();
    metalLayer.device = device;
    commandQueue = [device newCommandQueue];
    
    dispatch_semaphore_t inflightSemaphore = dispatch_semaphore_create(1);
    
    id<MTLBuffer> cubeBuffer = nil;
    uint32_t cubeVertexCount = 0;
    {
        std::vector<Vertex> vertices;
        generateCube(0, 0, 0, 1, false, [&](const float* pos, const float* normal, const float* uvs)
        {
            Vertex v;
            memcpy(v.pos, pos, sizeof(float) * 3);
            memcpy(v.norm, normal, sizeof(float) * 3);
            vertices.emplace_back(std::move(v));
        });
        cubeVertexCount = (uint32_t)vertices.size();
        
        cubeBuffer = [device newBufferWithLength:sizeof(Vertex) * vertices.size() options:MTLResourceStorageModePrivate];
        id<MTLBuffer> stagingBuffer1 = [device newBufferWithLength:sizeof(Vertex) * vertices.size() options:MTLResourceStorageModeShared];
        memcpy(stagingBuffer1.contents, vertices.data(), sizeof(Vertex) * vertices.size());
        id<MTLCommandBuffer> commandBuffer =  [commandQueue commandBuffer];
        id<MTLBlitCommandEncoder> blitEncoder = [commandBuffer blitCommandEncoder];
        [blitEncoder copyFromBuffer:stagingBuffer1 sourceOffset:0 toBuffer:cubeBuffer destinationOffset:0 size:sizeof(Vertex) * vertices.size()];
        [blitEncoder endEncoding];
        [commandBuffer commit];
    }
    
    id<MTLBuffer> viewConstants = [device newBufferWithLength:sizeof(ViewConstants) options:MTLResourceOptionCPUCacheModeWriteCombined];
    viewConstants.label = @"viewConstants";
    id<MTLBuffer> shadowViewConstants = [device newBufferWithLength:sizeof(ViewConstants) options:MTLResourceOptionCPUCacheModeDefault];
    shadowViewConstants.label = @"shadowViewConstants";
    id<MTLBuffer> gridMeshConstants = [device newBufferWithLength:sizeof(MeshConstants) options:MTLResourceOptionCPUCacheModeDefault];
    gridMeshConstants.label = @"gridMeshConstants";
    id<MTLBuffer> cubeMeshConstants = [device newBufferWithLength:sizeof(MeshConstants) options:MTLResourceOptionCPUCacheModeDefault];
    cubeMeshConstants.label = @"cubeMeshConstants";
    
    std::string result;
    readFileContents("/Users/sturm/sturm_projects/rendergraph/rendergraph/test.metal", &result);
    NSError* error = nil;
    MTLCompileOptions* options = [[MTLCompileOptions alloc] init];
    options.languageVersion    = MTLLanguageVersion1_1;
    id<MTLLibrary> library = [device newLibraryWithSource:[NSString stringWithUTF8String:result.c_str()] options:options error:&error];
    
    
    id<MTLRenderPipelineState> pipelineState = nil;
    {
        id<MTLFunction> vertexFunction = [library newFunctionWithName:@"blinn_vertex"];
        id<MTLFunction> fragmentFunction = [library newFunctionWithName:@"blinn_frag"];
        MTLRenderPipelineDescriptor* rpd = [[MTLRenderPipelineDescriptor alloc] init];
        
        MTLVertexDescriptor* vertexDescriptor = [MTLVertexDescriptor vertexDescriptor];
        vertexDescriptor.attributes[0].bufferIndex = 0;
        vertexDescriptor.attributes[0].offset = 0;
        vertexDescriptor.attributes[0].format = MTLVertexFormatFloat3;
        vertexDescriptor.attributes[1].bufferIndex = 0;
        vertexDescriptor.attributes[1].offset = sizeof(float) * 3;
        vertexDescriptor.attributes[1].format = MTLVertexFormatFloat3;        
        vertexDescriptor.layouts[0].stride = sizeof(Vertex);
        vertexDescriptor.layouts[0].stepRate = 1;
        vertexDescriptor.layouts[0].stepFunction = MTLVertexStepFunctionPerVertex;
        
        rpd.vertexDescriptor = vertexDescriptor;
        rpd.vertexFunction = vertexFunction;
        rpd.fragmentFunction = fragmentFunction;
        rpd.colorAttachments[0].pixelFormat = MTLPixelFormatRGBA32Float;
        rpd.colorAttachments[1].pixelFormat = MTLPixelFormatRGBA32Float;
        rpd.depthAttachmentPixelFormat = MTLPixelFormatDepth32Float;
        rpd.inputPrimitiveTopology = MTLPrimitiveTopologyClassTriangle;
        
        error = nil;
        MTLRenderPipelineReflection* reflection = nil;
        MTLPipelineOption pipelineOptions = MTLPipelineOptionBufferTypeInfo | MTLPipelineOptionArgumentInfo;
        pipelineState = [device newRenderPipelineStateWithDescriptor:rpd options:pipelineOptions reflection:&reflection error:&error];
    }
    
    id<MTLRenderPipelineState> gridPipelineState = nil;
    {
        id<MTLFunction> vertexFunction = [library newFunctionWithName:@"disp_vertex"];
        id<MTLFunction> fragmentFunction = [library newFunctionWithName:@"blinn_frag"];
        MTLRenderPipelineDescriptor* rpd = [[MTLRenderPipelineDescriptor alloc] init];
        
        MTLVertexDescriptor* vertexDescriptor = [MTLVertexDescriptor vertexDescriptor];
        vertexDescriptor.attributes[0].bufferIndex = 0;
        vertexDescriptor.attributes[0].offset = 0;
        vertexDescriptor.attributes[0].format = MTLVertexFormatFloat3;
        vertexDescriptor.attributes[1].bufferIndex = 0;
        vertexDescriptor.attributes[1].offset = sizeof(float) * 3;
        vertexDescriptor.attributes[1].format = MTLVertexFormatFloat3;
        vertexDescriptor.attributes[2].bufferIndex = 0;
        vertexDescriptor.attributes[2].offset = vertexDescriptor.attributes[1].offset + sizeof(float) * 3;
        vertexDescriptor.attributes[2].format = MTLVertexFormatFloat2;
        vertexDescriptor.layouts[0].stride = sizeof(Vertex2);
        vertexDescriptor.layouts[0].stepRate = 1;
        vertexDescriptor.layouts[0].stepFunction = MTLVertexStepFunctionPerVertex;
        
        rpd.vertexDescriptor = vertexDescriptor;
        rpd.vertexFunction = vertexFunction;
        rpd.fragmentFunction = fragmentFunction;
        rpd.colorAttachments[0].pixelFormat = MTLPixelFormatRGBA32Float;
        rpd.colorAttachments[1].pixelFormat = MTLPixelFormatRGBA32Float;
        rpd.depthAttachmentPixelFormat = MTLPixelFormatDepth32Float;
        rpd.inputPrimitiveTopology = MTLPrimitiveTopologyClassTriangle;
        
        error = nil;
        MTLRenderPipelineReflection* reflection = nil;
        MTLPipelineOption pipelineOptions = MTLPipelineOptionBufferTypeInfo | MTLPipelineOptionArgumentInfo;
        gridPipelineState = [device newRenderPipelineStateWithDescriptor:rpd options:pipelineOptions reflection:&reflection error:&error];
    }
    
    id<MTLRenderPipelineState> shadowPipelineState = nil;
    {
        id<MTLFunction> vertexFunction = [library newFunctionWithName:@"blinn_vertex"];
        id<MTLFunction> fragmentFunction = [library newFunctionWithName:@"linear_depth"];
        
        MTLRenderPipelineDescriptor* rpd = [[MTLRenderPipelineDescriptor alloc] init];
        
        MTLVertexDescriptor* vertexDescriptor = [MTLVertexDescriptor vertexDescriptor];
        vertexDescriptor.attributes[0].bufferIndex = 0;
        vertexDescriptor.attributes[0].offset = 0;
        vertexDescriptor.attributes[0].format = MTLVertexFormatFloat3;
        vertexDescriptor.attributes[1].bufferIndex = 0;
        vertexDescriptor.attributes[1].offset = sizeof(float) * 3;
        vertexDescriptor.attributes[1].format = MTLVertexFormatFloat3;
        vertexDescriptor.layouts[0].stride = sizeof(Vertex);
        vertexDescriptor.layouts[0].stepRate = 1;
        vertexDescriptor.layouts[0].stepFunction = MTLVertexStepFunctionPerVertex;
        
        rpd.vertexDescriptor = vertexDescriptor;
        rpd.vertexFunction = vertexFunction;
        rpd.fragmentFunction = fragmentFunction;
        rpd.colorAttachments[0].pixelFormat = MTLPixelFormatR32Float;
        rpd.depthAttachmentPixelFormat = MTLPixelFormatDepth32Float;
        rpd.inputPrimitiveTopology = MTLPrimitiveTopologyClassTriangle;
        
        error = nil;
        MTLRenderPipelineReflection* reflection = nil;
        MTLPipelineOption pipelineOptions = MTLPipelineOptionBufferTypeInfo | MTLPipelineOptionArgumentInfo;
        shadowPipelineState = [device newRenderPipelineStateWithDescriptor:rpd options:pipelineOptions reflection:&reflection error:&error];
    }
    
    id<MTLRenderPipelineState> gridShadowPipelineState = nil;
    {
        id<MTLFunction> vertexFunction = [library newFunctionWithName:@"blinn_vertex2"];
        id<MTLFunction> fragmentFunction = [library newFunctionWithName:@"linear_depth"];
        
        MTLRenderPipelineDescriptor* rpd = [[MTLRenderPipelineDescriptor alloc] init];
        
        MTLVertexDescriptor* vertexDescriptor = [MTLVertexDescriptor vertexDescriptor];
        vertexDescriptor.attributes[0].bufferIndex = 0;
        vertexDescriptor.attributes[0].offset = 0;
        vertexDescriptor.attributes[0].format = MTLVertexFormatFloat3;
        vertexDescriptor.attributes[1].bufferIndex = 0;
        vertexDescriptor.attributes[1].offset = sizeof(float) * 3;
        vertexDescriptor.attributes[1].format = MTLVertexFormatFloat3;
        vertexDescriptor.attributes[2].bufferIndex = 0;
        vertexDescriptor.attributes[2].offset = vertexDescriptor.attributes[1].offset + sizeof(float) * 3;
        vertexDescriptor.attributes[2].format = MTLVertexFormatFloat2;
        vertexDescriptor.layouts[0].stride = sizeof(Vertex2);
        vertexDescriptor.layouts[0].stepRate = 1;
        vertexDescriptor.layouts[0].stepFunction = MTLVertexStepFunctionPerVertex;
        
        rpd.vertexDescriptor = vertexDescriptor;
        rpd.vertexFunction = vertexFunction;
        rpd.fragmentFunction = fragmentFunction;
        rpd.colorAttachments[0].pixelFormat = MTLPixelFormatR32Float;
        rpd.depthAttachmentPixelFormat = MTLPixelFormatDepth32Float;
        rpd.inputPrimitiveTopology = MTLPrimitiveTopologyClassTriangle;
        
        error = nil;
        MTLRenderPipelineReflection* reflection = nil;
        MTLPipelineOption pipelineOptions = MTLPipelineOptionBufferTypeInfo | MTLPipelineOptionArgumentInfo;
        gridShadowPipelineState = [device newRenderPipelineStateWithDescriptor:rpd options:pipelineOptions reflection:&reflection error:&error];
    }
    
    id<MTLRenderPipelineState> emptyPipelineState = nil;
    {
        id<MTLFunction> vertexFunction = [library newFunctionWithName:@"fullscreen_quad"];
        id<MTLFunction> fragmentFunction = [library newFunctionWithName:@"composition"];
        
        MTLRenderPipelineDescriptor* rpd = [[MTLRenderPipelineDescriptor alloc] init];
        
        rpd.vertexDescriptor = nil;
        rpd.vertexFunction = vertexFunction;
        rpd.fragmentFunction = fragmentFunction;
        rpd.colorAttachments[0].pixelFormat = MTLPixelFormatBGRA8Unorm;
        rpd.inputPrimitiveTopology = MTLPrimitiveTopologyClassTriangle;
        
        error = nil;
        MTLRenderPipelineReflection* reflection = nil;
        MTLPipelineOption pipelineOptions = MTLPipelineOptionBufferTypeInfo | MTLPipelineOptionArgumentInfo;
        emptyPipelineState = [device newRenderPipelineStateWithDescriptor:rpd options:pipelineOptions reflection:&reflection error:&error];
    }
    
    MTLDepthStencilDescriptor* dsd = [[MTLDepthStencilDescriptor alloc] init];
    dsd.depthWriteEnabled = true;
    dsd.depthCompareFunction = MTLCompareFunctionLess;
    
    id<MTLDepthStencilState> depthStencilState = [device newDepthStencilStateWithDescriptor:dsd];
    
    AttachmentDesc albedoDesc, normalDesc, finalDesc, depthDesc, shadowDesc;
    albedoDesc.format = RenderFormat::RGBA32Float;
    albedoDesc.loadAction = MTLLoadActionClear;
    albedoDesc.storeAction = MTLStoreActionStore;
    
    normalDesc.format = RenderFormat::RGBA32Float;
    normalDesc.loadAction = MTLLoadActionClear;
    normalDesc.storeAction = MTLStoreActionStore;
    
    shadowDesc.format = RenderFormat::R32Float;
    shadowDesc.loadAction = MTLLoadActionClear;
    shadowDesc.storeAction = MTLStoreActionStore;
    
    depthDesc.format = RenderFormat::Depth32Float;
    depthDesc.loadAction = MTLLoadActionClear;
    depthDesc.storeAction = MTLStoreActionStore;
    
    finalDesc.sizeType = SizeType::Absolute;
    finalDesc.width = 1024;
    finalDesc.height = 768;
    finalDesc.format = RenderFormat::BGRA8Unorm;
    finalDesc.loadAction = MTLLoadActionClear;
    finalDesc.clearColor = MTLClearColorMake(1, 0, 0, 1);
    finalDesc.storeAction = MTLStoreActionDontCare;
    
    uint32_t gridVertexCount = 0;
    id<MTLBuffer> gridBuffer = nil;
    id<MTLTexture> displacement = nil;
    id<MTLTexture> displacementUnormView = nil;
    {
        int x,y,n;
        unsigned char* data = stbi_load("/Users/sturm/Downloads/Yosemite\ terrain/Yosemite\ Height\ Map\ \(Merged\).png", &x, &y, &n, 0);
        std::vector<Vertex2> vertices;
        generateGrid(0, 0, 0, 10, 10, 4, 4,
                     [&](const float* pos, const float* normal, const float* uvs)
                     {
                         Vertex2 v;
                         memcpy(v.pos, pos, sizeof(float) * 3);
                         memcpy(v.norm, normal, sizeof(float) * 3);
                         memcpy(v.uv, uvs, sizeof(float) * 2);
                         vertices.emplace_back(std::move(v));
                     });
        gridVertexCount = (uint32_t)vertices.size();
        
        
        
        MTLTextureDescriptor* mtlDesc = [[MTLTextureDescriptor alloc] init];
        mtlDesc.textureType           = MTLTextureType2D;
        mtlDesc.width                 = x;
        mtlDesc.height                = y;
        mtlDesc.pixelFormat           = MTLPixelFormatR8Uint;
        mtlDesc.depth                 = 1;
        mtlDesc.mipmapLevelCount      = 1;
        mtlDesc.sampleCount           = 1;
        mtlDesc.arrayLength           = 1;
        mtlDesc.cpuCacheMode          = MTLCPUCacheModeDefaultCache;
        mtlDesc.storageMode           = MTLStorageModePrivate;
        mtlDesc.usage                 = MTLTextureUsageShaderRead | MTLTextureUsagePixelFormatView;
        displacement = [device newTextureWithDescriptor:mtlDesc];
        displacement.label = @"disp";
        
        displacementUnormView = [displacement newTextureViewWithPixelFormat:MTLPixelFormatR8Unorm];
        
        mtlDesc.storageMode           = MTLStorageModeManaged;
        id<MTLTexture> staging = [device newTextureWithDescriptor:mtlDesc];
        [staging replaceRegion:MTLRegionMake2D(0, 0, x, y) mipmapLevel:0 withBytes:data bytesPerRow:x];
        
        id<MTLCommandBuffer> commandBuffer =  [commandQueue commandBuffer];
        id<MTLBlitCommandEncoder> blitEncoder = [commandBuffer blitCommandEncoder];
        [blitEncoder copyFromTexture:staging
                         sourceSlice:0 sourceLevel:0
                        sourceOrigin:MTLOriginMake(0, 0, 0)
                          sourceSize:MTLSizeMake(x, y, 1)
                           toTexture:displacement
                    destinationSlice:0
                    destinationLevel:0
                   destinationOrigin:MTLOriginMake(0, 0, 0)];

        gridBuffer = [device newBufferWithLength:vertices.size() * sizeof(Vertex2) options:MTLResourceStorageModePrivate];
        id<MTLBuffer> gridStaging = [device newBufferWithLength:vertices.size() * sizeof(Vertex2) options:MTLResourceStorageModeShared];
        memcpy(gridStaging.contents, vertices.data(), vertices.size() * sizeof(Vertex2));

        [blitEncoder copyFromBuffer:gridStaging sourceOffset:0 toBuffer:gridBuffer destinationOffset:0 size:vertices.size() * sizeof(Vertex2)];
        [blitEncoder endEncoding];
        [commandBuffer commit];
        stbi_image_free(data);
    }
    
    
    RenderGraph graph;
    RenderPass* shadowPass = graph.addRenderPass("shadowMap",
                                                 [&](RenderPassBuilder* builder)
                                                 {
                                                     builder->addColorOutput("shadowMap", shadowDesc);
                                                     builder->setDepthStencilOutput("depth", depthDesc);
                                                 },
                                                 [&](id<MTLRenderCommandEncoder> encoder, const std::map<std::string, id<MTLTexture>>& inputs)
                                                 {
                                                     [encoder setRenderPipelineState:gridShadowPipelineState];
                                                     [encoder setFrontFacingWinding:MTLWindingCounterClockwise];
                                                     [encoder setCullMode:MTLCullModeNone];
                                                     [encoder setDepthStencilState:depthStencilState];
                                                     [encoder setTriangleFillMode:MTLTriangleFillModeLines];
                                                     [encoder setVertexBuffer:gridBuffer offset:0 atIndex:0];
                                                     [encoder setVertexBuffer:shadowViewConstants offset:0 atIndex:1];
                                                     [encoder setVertexBuffer:gridMeshConstants offset:0 atIndex:2];
                                                     [encoder drawPrimitives:MTLPrimitiveTypeTriangle vertexStart:0 vertexCount:gridVertexCount];
                                                     
                                                     [encoder setRenderPipelineState:shadowPipelineState];
                                                     [encoder setDepthStencilState:depthStencilState];
                                                     [encoder setFrontFacingWinding:MTLWindingCounterClockwise];
                                                     [encoder setCullMode:MTLCullModeBack];
                                                     [encoder setTriangleFillMode:MTLTriangleFillModeLines];
                                                     [encoder setVertexBuffer:cubeBuffer offset:0 atIndex:0];
                                                     [encoder setVertexBuffer:shadowViewConstants offset:0 atIndex:1];
                                                     [encoder setVertexBuffer:cubeMeshConstants offset:0 atIndex:2];
                                                     [encoder drawPrimitives:MTLPrimitiveTypeTriangle vertexStart:0 vertexCount:cubeVertexCount];
                                                 });
    
    RenderPass* gbufferPass = graph.addRenderPass("gbuffer",
                                                 [&](RenderPassBuilder* builder)
                                                 {
                                                     builder->addColorOutput("albedo", albedoDesc);
                                                     builder->addColorOutput("normal", normalDesc);
                                                     builder->setDepthStencilOutput("depth", depthDesc);
                                                 },
                                                 [&](id<MTLRenderCommandEncoder> encoder, const std::map<std::string, id<MTLTexture>>& inputs)
                                                 {
                                                     [encoder setRenderPipelineState:pipelineState];
                                                     [encoder setFrontFacingWinding:MTLWindingCounterClockwise];
                                                     [encoder setCullMode:MTLCullModeBack];
                                                     [encoder setDepthStencilState:depthStencilState];
                                                     [encoder setTriangleFillMode:MTLTriangleFillModeFill];
                                                     [encoder setVertexBuffer:cubeBuffer offset:0 atIndex:0];
                                                     [encoder setVertexBuffer:viewConstants offset:0 atIndex:1];
                                                     [encoder setVertexBuffer:cubeMeshConstants offset:0 atIndex:2];
                                                     [encoder drawPrimitives:MTLPrimitiveTypeTriangle vertexStart:0 vertexCount:cubeVertexCount];
                                                     
                                                     
                                                     MTLSamplerDescriptor *samplerDescriptor = [MTLSamplerDescriptor new];
                                                     samplerDescriptor.minFilter = MTLSamplerMinMagFilterNearest;
                                                     samplerDescriptor.magFilter = MTLSamplerMinMagFilterLinear;
                                                     samplerDescriptor.sAddressMode = MTLSamplerAddressModeRepeat;
                                                     samplerDescriptor.tAddressMode = MTLSamplerAddressModeRepeat;
                                                     
                                                     id<MTLSamplerState> sampler = [device newSamplerStateWithDescriptor:samplerDescriptor];
                                                     
                                                     [encoder setRenderPipelineState:gridPipelineState];
                                                     [encoder setFrontFacingWinding:MTLWindingCounterClockwise];
                                                     [encoder setCullMode:MTLCullModeNone];
                                                     [encoder setDepthStencilState:depthStencilState];
                                                     [encoder setTriangleFillMode:MTLTriangleFillModeFill];
                                                     [encoder setVertexTexture:displacementUnormView atIndex:0];
                                                     [encoder setVertexSamplerState:sampler atIndex:0];
                                                     [encoder setVertexBuffer:gridBuffer offset:0 atIndex:0];
                                                     [encoder setVertexBuffer:viewConstants offset:0 atIndex:1];
                                                     [encoder setVertexBuffer:gridMeshConstants offset:0 atIndex:2];
                                                     [encoder drawPrimitives:MTLPrimitiveTypeTriangle vertexStart:0 vertexCount:gridVertexCount];
                                                 });
    RenderPass* lightingPass = graph.addRenderPass("lighting",
                                                   [&](RenderPassBuilder* builder)
                                                   {
                                                       builder->addColorOutput("final", finalDesc);
                                                       builder->addAttachmentInput("albedo");
                                                       builder->addAttachmentInput("normal");
                                                       builder->addAttachmentInput("shadowMap");
                                                   },
                                                   [&](id<MTLRenderCommandEncoder> encoder, const std::map<std::string, id<MTLTexture>>& inputs)
                                                   {
                                                       [encoder setRenderPipelineState:emptyPipelineState];
                                                       [encoder setFrontFacingWinding:MTLWindingCounterClockwise];
                                                       [encoder setCullMode:MTLCullModeBack];
                                                       [encoder setTriangleFillMode:MTLTriangleFillModeFill];                                                       
//
                                                       MTLViewport viewport;
                                                       viewport.originX = 0;
                                                       viewport.originY = 0;
                                                       viewport.width = 1024.0 / 2.0;
                                                       viewport.height = 768.0 / 2.0;
                                                       viewport.znear = 0.0;
                                                       viewport.zfar = 1.0;
                                                       [encoder setViewport:viewport];
                                                       [encoder setFragmentTexture:inputs.at("albedo") atIndex:0];
                                                       [encoder drawPrimitives:MTLPrimitiveTypeTriangle vertexStart:0 vertexCount:3];
                                                       
                                                       viewport.originX = 1024.0 / 2.0;
                                                       viewport.originY = 0;
                                                       [encoder setViewport:viewport];
                                                       [encoder setFragmentTexture:inputs.at("normal") atIndex:0];
                                                       [encoder drawPrimitives:MTLPrimitiveTypeTriangle vertexStart:0 vertexCount:3];

                                                       viewport.originX = 0;
                                                       viewport.originY = 768.0 / 2.0;
                                                       [encoder setViewport:viewport];
                                                       [encoder setFragmentTexture:inputs.at("shadowMap") atIndex:0];
                                                       [encoder drawPrimitives:MTLPrimitiveTypeTriangle vertexStart:0 vertexCount:3];
                                                   });
//    graph.addRenderPass("blur",
//                        [&](RenderPassBuilder* builder)
//                        {
//                            builder->addColorOutput("final", finalDesc);
//                            builder->addAttachmentInput("final");
//                        },
//                        [&](id<MTLRenderCommandEncoder> encoder, const std::map<std::string, id<MTLTexture>>& inputs)
//                        {
//
//                        });
//
    
    graph.setBackbuffer("final", finalDesc);
    
    // RenderQueue per RenderPass
    // Build draw items and insert into renderqueue
    
    SDL_Event _e;
    uint32_t i = 0;
    while (true) {
        while (SDL_PollEvent(&_e) != 0) {}
        @autoreleasepool {
            dispatch_semaphore_wait(inflightSemaphore, DISPATCH_TIME_FOREVER);
//            printf("begin:%d\n", i);
            id<CAMetalDrawable> drawable = metalLayer.nextDrawable;
            
            id<MTLCommandBuffer> commandBuffer = [commandQueue commandBuffer];
            
            __block int idx = i;
            __block dispatch_semaphore_t blockSemaphore = inflightSemaphore;
            [commandBuffer addCompletedHandler:^(id<MTLCommandBuffer> buffer) {
//                printf("end:%d\n", idx);
                dispatch_semaphore_signal(blockSemaphore);
            }];
                                                
            
            
            {
                ViewConstants* write = reinterpret_cast<ViewConstants*>(viewConstants.contents);
                write->proj = proj;
                write->view = view;
            }
            
            {
                ViewConstants* write = reinterpret_cast<ViewConstants*>(shadowViewConstants.contents);
                write->proj = proj;
                write->view = shadowView;
            }
            
            {
                cubeWorld = glm::rotate(cubeWorld, 0.01f, glm::vec3(0, 1, 0));
                cubeWorld = glm::rotate(cubeWorld, 0.005f, glm::normalize(glm::vec3(1, 0, 0)));
                MeshConstants* write = reinterpret_cast<MeshConstants*>(cubeMeshConstants.contents);
                write->world = cubeWorld;
            }
            
            {
                MeshConstants* write = reinterpret_cast<MeshConstants*>(gridMeshConstants.contents);
                write->world = gridWorld;
            }
            
            std::string backbufferStr = "backbuffer_"+std::to_string(idx);
            drawable.texture.label = [NSString stringWithUTF8String:backbufferStr.c_str()];
            graph.execute(device, commandBuffer, drawable.texture);
            
            [commandBuffer presentDrawable:drawable];
            [commandBuffer commit];
            
            
        }
        ++i;
    }
    
    return 0;
}
