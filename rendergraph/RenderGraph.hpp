//
//  RenderGraph.hpp
//  rendergraph
//
//  Created by Eugene Sturm on 4/12/18.
//  Copyright © 2018 Eugene Sturm. All rights reserved.
//

#ifndef RenderGraph_hpp
#define RenderGraph_hpp

#import <Metal/Metal.h>
#include <string>
#include <map>
#include <vector>
#include "AttachmentDesc.h"

class RenderPass;
class RenderGraphResource;
class ResourceRegistry;
class RenderPassBuilder;

using RenderGraphSetupDelegate = std::function<void(RenderPassBuilder*)>;
using RenderGraphExecutionDelegate = std::function<void(id<MTLRenderCommandEncoder>, const std::map<std::string, id<MTLTexture>>&)>;

constexpr bool isDepthFormat(MTLPixelFormat pixelFormat)
{
    return pixelFormat == MTLPixelFormatDepth16Unorm || pixelFormat == MTLPixelFormatDepth32Float || pixelFormat == MTLPixelFormatDepth24Unorm_Stencil8
    || pixelFormat == MTLPixelFormatDepth32Float_Stencil8;
}

constexpr bool isStencilFormat(MTLPixelFormat pixelFormat)
{
    return pixelFormat == MTLPixelFormatStencil8 || pixelFormat == MTLPixelFormatX24_Stencil8 || pixelFormat == MTLPixelFormatX32_Stencil8
    || pixelFormat == MTLPixelFormatDepth24Unorm_Stencil8 || pixelFormat == MTLPixelFormatDepth32Float_Stencil8;
}

class Adapter {
public:
    static MTLPixelFormat toMTL(RenderFormat format) {
        switch (format) {
            case RenderFormat::Undefined:
                return MTLPixelFormatInvalid;
            case RenderFormat::BGRA8Unorm:
                return MTLPixelFormatBGRA8Unorm;
            case RenderFormat::Depth32Float:
                return MTLPixelFormatDepth32Float;
            case RenderFormat::RGBA32Float:
                return MTLPixelFormatRGBA32Float;
            case RenderFormat::R32Float:
                return MTLPixelFormatR32Float;
        }
        return MTLPixelFormatInvalid;
    }
};

class RenderGraph
{
public:
    RenderGraph();
    ~RenderGraph();
    
    RenderPass* addRenderPass(const std::string& name, RenderGraphSetupDelegate setup, RenderGraphExecutionDelegate execute);
    void setBackbuffer(const std::string& name, const AttachmentDesc& desc);
    
    void execute(id<MTLDevice> device, id<MTLCommandBuffer> commandBuffer, id<MTLTexture> backbuffer);
    
private:
    void validate();
    std::vector<const RenderPass*> bake();
private:
    std::string _backbuffer;
    AttachmentDesc _backbufferDesc;
    
    std::unique_ptr<ResourceRegistry> _registry;
    
    std::vector<std::unique_ptr<RenderPass>> _passes;
    std::map<std::string, RenderPass*> _passMap;
    std::map<const RenderGraphResource*, id<MTLTexture>> _resourceTextureMap;
};

#endif /* RenderGraph_hpp */
