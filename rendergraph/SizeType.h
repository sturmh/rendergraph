//
//  SizeType.h
//  rendergraph
//
//  Created by Eugene Sturm on 4/12/18.
//  Copyright © 2018 Eugene Sturm. All rights reserved.
//

#ifndef SizeType_h
#define SizeType_h

#include <stdint.h>

enum class SizeType : uint8_t
{
    SwapchainRelative,
    Absolute
};

#endif /* SizeType_h */
