//
//  RenderPassBuilder.cpp
//  rendergraph
//
//  Created by Eugene Sturm on 4/12/18.
//  Copyright © 2018 Eugene Sturm. All rights reserved.
//

#include "RenderPassBuilder.hpp"
#include "RenderPass.hpp"
#include "ResourceRegistry.hpp"
#include "RenderGraphResource.hpp"

RenderPassBuilder::RenderPassBuilder(ResourceRegistry* registry, const std::string& name, RenderGraphExecutionDelegate executionDelegate)
: _registry(registry)
, _delegate(executionDelegate)
, _name(name)
{
}
void RenderPassBuilder::addColorOutput(const std::string& name, const AttachmentDesc& desc)
{
    RenderGraphResource* resource = _registry->getTextureResource(name);
    if (resource == nullptr) {
        resource = _registry->createTextureResource(name, desc);
    } else {
        // assert resource->desc() == desc
    }
    
    _colorOutputs.push_back(resource);
    
}
void RenderPassBuilder::addAttachmentInput(const std::string& name)
{
    RenderGraphResource* resource = _registry->getTextureResource(name);
    if (resource) {
        _attachmentInputs.push_back(resource);
    }
}
void RenderPassBuilder::setDepthStencilInput(const std::string& name)
{
    RenderGraphResource* resource = _registry->getTextureResource(name);
    if (resource) {
        // assert is depth stencil
        _depthStencilInput = resource;
    }
}
void RenderPassBuilder::setDepthStencilOutput(const std::string& name, const AttachmentDesc& desc)
{
    RenderGraphResource* resource = _registry->getTextureResource(name);
    if (resource == nullptr) {
        resource = _registry->createTextureResource(name, desc);
    } else {
        // assert resource->desc() == desc
    }
    _depthStencilOutput = resource;
}

RenderPass* RenderPassBuilder::build()
{    
    RenderPass* pass = new RenderPass(_delegate, std::move(_colorOutputs), _depthStencilOutput, std::move(_attachmentInputs), _depthStencilInput, _name);
    return pass;
}
