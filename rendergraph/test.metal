#include <metal_stdlib>
#include <metal_types.h>

using namespace metal;

struct VertexIn
{
    float3 position[[attribute(0)]];
    float3 normals[[attribute(1)]];
};

struct VertexIn2
{
    float3 position[[attribute(0)]];
    float3 normals[[attribute(1)]];
    float2 uv[[attribute(2)]];
};

struct VertexOut
{
    float4 position[[position]];
    float3 normal;
};

struct VertexOut2
{
    float4 position[[position]];
    float3 normal;
    float2 uv;
};

struct ViewConstants
{
    float4x4 view;
    float4x4 proj;
};

struct ObjectConstants
{
    float4x4 world;
};

struct FragOutput
{
    float4 albedo [[color(0)]];
    float4 normal [[color(1)]];
};

vertex VertexOut2 fullscreen_quad(uint vid [[vertex_id]])
{
    VertexOut2 vout;
    vout.uv = float2((vid << 1) & 2, vid & 2);
    vout.position = float4(vout.uv * 2.0f + -1.0f, 0.0f, 1.0f);
    return vout;
}

fragment float4 composition(VertexOut2 varyingInput[[stage_in]], texture2d<float> diffuse[[texture(0)]])
{
    constexpr sampler s(coord::normalized,
                        address::clamp_to_zero,
                        filter::linear);
    
    float4 c = diffuse.sample(s, float2(varyingInput.uv.x, (1.0  - varyingInput.uv.y)));
    return float4(c.r, c.g, c.b, 1);
}

vertex VertexOut blinn_vertex(VertexIn attributes[[stage_in]],
                              constant ViewConstants& view[[buffer(1)]],
                              constant ObjectConstants& obj[[buffer(2)]])
{
    float4 worldPos = obj.world * float4(attributes.position.x, attributes.position.y, attributes.position.z, 1.f);
    
    VertexOut outputValue;
    outputValue.position = view.proj * view.view * worldPos;
    outputValue.normal = attributes.normals;
    
    return outputValue;
}

vertex VertexOut blinn_vertex2(VertexIn2 attributes[[stage_in]],
                              constant ViewConstants& view[[buffer(1)]],
                              constant ObjectConstants& obj[[buffer(2)]])
{
    float4 worldPos = obj.world * float4(attributes.position.x, attributes.position.y, attributes.position.z, 1.f);
    
    VertexOut outputValue;
    outputValue.position = view.proj * view.view * worldPos;
    outputValue.normal = attributes.normals;
    
    return outputValue;
}

vertex VertexOut disp_vertex(VertexIn2 attributes[[stage_in]],
                             texture2d<float> disp[[texture(0)]],
                             sampler s2 [[sampler(0)]],
                             constant ViewConstants& view[[buffer(1)]],
                             constant ObjectConstants& obj[[buffer(2)]])
{
    constexpr sampler s(coord::normalized,
                        address::repeat,
                        filter::nearest);
    
    constexpr sampler s3(coord::normalized,
                        s_address::repeat,
                        t_address::repeat,
                        min_filter::nearest,
                        mag_filter::linear);
    
    
    float4 worldPos = obj.world * float4(attributes.position.x, attributes.position.y, attributes.position.z, 1.f);
    
    VertexOut outputValue;
    outputValue.position = view.proj * view.view * worldPos;
    outputValue.normal = attributes.normals;
    outputValue.normal.x = disp.sample(s3, float2(0, 0)).x;
    return outputValue;
}

vertex VertexOut2 blinn_vertex22(VertexIn2 attributes[[stage_in]],
                               constant ViewConstants& view[[buffer(1)]],
                               constant ObjectConstants& obj[[buffer(2)]])
{
    float4 worldPos = obj.world * float4(attributes.position.x, attributes.position.y, attributes.position.z, 1.f);
    
    VertexOut2 outputValue;
    outputValue.position = view.proj * view.view * worldPos;
    outputValue.normal = attributes.normals;
    outputValue.uv = attributes.uv;
    
    return outputValue;
}

fragment FragOutput blinn_frag2(VertexOut2 varyingInput[[stage_in]],
                                texture2d<float> diffuse[[texture(0)]])
{
    constexpr sampler s(coord::normalized,
                        address::clamp_to_zero,
                        filter::linear);
    
    float4 c = diffuse.sample(s, varyingInput.uv);
    
    FragOutput output;
    
    output.albedo = float4(c.r, c.g, c.b, 1);
    output.normal.rgb = varyingInput.normal;
    output.normal.a = 1.0f;
    
    return output;
};

fragment FragOutput blinn_frag(VertexOut varyingInput[[stage_in]])
{
    FragOutput output;
    
    output.albedo = varyingInput.position;
    output.normal.rgb = varyingInput.normal;
    output.normal.a = 1.0f;
    
    return output;
};

fragment float linear_depth(VertexOut varyingInput[[stage_in]])
{
    return varyingInput.position.z;
};
