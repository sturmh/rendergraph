//
//  RenderPass.hpp
//  rendergraph
//
//  Created by Eugene Sturm on 4/12/18.
//  Copyright © 2018 Eugene Sturm. All rights reserved.
//

#ifndef RenderPass_hpp
#define RenderPass_hpp

#include <functional>
#include <vector>
#include <string>
#include <map>
#include "AttachmentDesc.h"
#include <Metal/Metal.h>

class ResourceRegistry;
class RenderGraphResource;

using RenderGraphExecutionDelegate = std::function<void(id<MTLRenderCommandEncoder>, const std::map<std::string, id<MTLTexture>>&)>;

class RenderPass
{
public:
    RenderPass(RenderGraphExecutionDelegate executionDelegate,
               std::vector<const RenderGraphResource*>&& colorOutputs,
               const RenderGraphResource* depthStencilOutput,
               std::vector<const RenderGraphResource*>&& attachmentInputs,
               const RenderGraphResource* depthStencilInput,
               const std::string& name = "");
    
    void execute(const std::map<std::string, id<MTLTexture>>& inputs, id<MTLRenderCommandEncoder> encoder) const;
    
    const std::string& name() const;
    const RenderGraphResource* depthStencilOutput() const;
    const RenderGraphResource* depthStencilInput() const;
    const std::vector<const RenderGraphResource*>& colorOutputs() const;
    const std::vector<const RenderGraphResource*>& attachmentInputs() const;

private:
    std::string _name;
    RenderGraphExecutionDelegate _delegate;
    
    std::vector<const RenderGraphResource*> _colorOutputs;
    const RenderGraphResource* _depthStencilOutput { nullptr };
    
    std::vector<const RenderGraphResource*> _attachmentInputs;
    const RenderGraphResource* _depthStencilInput { nullptr };
};



#endif /* RenderPass_hpp */
