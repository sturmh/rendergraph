//
//  ResourceRegistry.cpp
//  rendergraph
//
//  Created by Eugene Sturm on 4/12/18.
//  Copyright © 2018 Eugene Sturm. All rights reserved.
//

#include "ResourceRegistry.hpp"
#include "RenderGraphResource.hpp"

RenderGraphResource* ResourceRegistry::getTextureResource(const std::string& name) const
{
    RenderGraphResource* resource = nullptr;
    auto it = _resources.find(name);
    if (it != end(_resources)) {
        resource = it->second.get();
    }
    
    return resource;
}

RenderGraphResource* ResourceRegistry::createTextureResource(const std::string& name, const AttachmentDesc& desc)
{
    RenderGraphResource* resource = getTextureResource(name);
    if (resource == nullptr) {
        resource = new RenderGraphResource(RenderGraphResourceType::Texture, desc, name);
        _resources.emplace(name, resource);
    }
    
    return resource;
}
