//
//  AttachmentDesc.h
//  rendergraph
//
//  Created by Eugene Sturm on 4/12/18.
//  Copyright © 2018 Eugene Sturm. All rights reserved.
//

#ifndef AttachmentDesc_h
#define AttachmentDesc_h

#include "SizeType.h"
#include "RenderFormat.h"
#import <Metal/Metal.h>

struct AttachmentDesc
{
    SizeType sizeType { SizeType::SwapchainRelative };
    float width { 1.f };
    float height { 1.f };
    MTLLoadAction loadAction { MTLLoadActionDontCare };
    MTLStoreAction storeAction { MTLStoreActionDontCare };
    RenderFormat format { RenderFormat::Undefined };
    
    MTLClearColor clearColor { MTLClearColorMake(0.0, 0.0, 0.0, 0.0) };
    float clearDepth { 1.f };
    float clearStencil { 0.f };
};


#endif /* AttachmentDesc_h */
