//
//  RenderFormat.h
//  rendergraph
//
//  Created by Eugene Sturm on 4/12/18.
//  Copyright © 2018 Eugene Sturm. All rights reserved.
//

#ifndef RenderFormat_h
#define RenderFormat_h

enum class RenderFormat : uint16_t
{
    Undefined,
    RGBA32Float,
    R32Float,
    BGRA8Unorm,
    Depth32Float,
};

#endif /* RenderFormat_h */
