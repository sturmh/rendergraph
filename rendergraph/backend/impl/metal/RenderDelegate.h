#pragma once

class RenderDelegate {
public:
    virtual void SubmitToGPU() = 0;
};
