//
//  MetalSwapChain.hpp
//  planet
//
//  Created by Eugene Sturm on 4/12/18.
//

#ifndef MetalSwapChain_hpp
#define MetalSwapChain_hpp

#import "RenderDevice.h"
#import <Metal/Metal.h>
#import <QuartzCore/CAMetalLayer.h>

namespace gfx
{
    class MetalSwapChain : public SwapChain
    {
    private:
        // Metal supports 3 concurrent drawables, but if the
        // swapchain is destroyed and rebuilt as part of resizing,
        // one will be held by the current display
        static constexpr uint32_t kImageCount = 2;
        
        dispatch_semaphore_t _inflightSemaphore;
        CAMetalLayer* _metalLayer;
        id<CAMetalDrawable> _currentDrawable;
        id<MTLCommandQueue> _commandQueue;
    public:
        MetalSwapChain(id<MTLCommandQueue> commandQueue, CAMetalLayer* metalLayer);
        
        virtual TextureId begin() final;
        virtual void present() final;
        
    };
}

#endif /* MetalSwapChain_hpp */
