//
//  MetalSwapChain.cpp
//  planet
//
//  Created by Eugene Sturm on 4/12/18.
//

#import "MetalSwapChain.h"

using namespace gfx;

MetalSwapChain::MetalSwapChain(id<MTLCommandQueue> commandQueue, CAMetalLayer* metalLayer)
: _metalLayer(metalLayer)
, _commandQueue(commandQueue)
, _inflightSemaphore(dispatch_semaphore_create(kImageCount))
{
    
}

TextureId MetalSwapChain::begin()
{
    dispatch_semaphore_wait(_inflightSemaphore, DISPATCH_TIME_FOREVER);
    _currentDrawable = [_metalLayer nextDrawable];    
    return 0;
}

void MetalSwapChain::present()
{
    id<MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];
    [commandBuffer enqueue];
    
    [commandBuffer pushDebugGroup:@"Present"];
    
    __block dispatch_semaphore_t blockSemaphore = _inflightSemaphore;
    [commandBuffer addCompletedHandler:^(id<MTLCommandBuffer> buffer) {
        dispatch_semaphore_signal(blockSemaphore);
    }];
    
    [commandBuffer presentDrawable:_currentDrawable];
    [commandBuffer popDebugGroup];
    [commandBuffer commit];
}
