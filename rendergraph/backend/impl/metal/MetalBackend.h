//
//  MetalBackend.h
//  planet
//
//  Created by Eugene Sturm on 4/12/18.
//

#ifndef MetalBackend_h
#define MetalBackend_h

#import "RenderDevice.h"

namespace gfx
{
    class MetalSwapChain;
    class MetalDevice;
    
    class MetalBackend : public RenderBackend
    {
    private:
        std::unique_ptr<MetalDevice> _device;
        std::vector<MetalSwapChain*> _swapChains;
    public:
        MetalBackend();
        
        virtual RenderDevice* getRenderDevice() final;
        virtual SwapChain* createSwapChainForWindow(const SwapChainDesc& swapChainDesc, RenderDevice* device, void* windowHandle) final;
    };
}
#endif /* MetalBackend_hpp */
