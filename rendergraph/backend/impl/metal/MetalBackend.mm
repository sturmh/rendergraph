//
//  MetalBackend.cpp
//  planet
//
//  Created by Eugene Sturm on 4/12/18.
//

#import "MetalBackend.h"
#import "MetalSwapChain.h"
#import "MetalDevice.h"
#import <AppKit/AppKit.h>
#import "MetalEnumAdapter.h"

@interface MetalView2 : NSView
@end

@implementation MetalView2

+ (Class)layerClass {
    return [CAMetalLayer class];
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        self.wantsLayer = YES;
        self.layer = [CAMetalLayer layer];
    }
    
    return self;
}

@end

using namespace gfx;

MetalBackend::MetalBackend()
{
    id<MTLDevice> device = MTLCreateSystemDefaultDevice();
    _device.reset(new MetalDevice(device));
}

RenderDevice* MetalBackend::getRenderDevice()
{
    return reinterpret_cast<RenderDevice*>(_device.get());
}

SwapChain* MetalBackend::createSwapChainForWindow(const SwapChainDesc& swapChainDesc, RenderDevice* device, void* windowHandle)
{
    NSWindow* window = reinterpret_cast<NSWindow*>(windowHandle);
    MetalDevice* metalDevice = reinterpret_cast<MetalDevice*>(device);
    
    MetalView2* view = [[MetalView2 alloc] initWithFrame:window.contentView.frame];
    [window.contentView addSubview:view];
    CAMetalLayer* metalLayer = (CAMetalLayer*)view.layer;
    metalLayer.device = metalDevice->getMTLDevice();
    metalLayer.pixelFormat = MetalEnumAdapter::toMTL(swapChainDesc.format);
    
    MetalSwapChain* swapChain = new MetalSwapChain(metalDevice->getMTLCommandQueue(), metalLayer);
    
    _swapChains.push_back(swapChain);
    
    return reinterpret_cast<SwapChain*>(swapChain);
}
