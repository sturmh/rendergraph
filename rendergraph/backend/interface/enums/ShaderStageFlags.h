#pragma once

namespace gfx {
    enum class ShaderStageFlags : uint8_t{
        None           = 0,
        VertexBit      = 1 << 0,
        TessControlBit = 1 << 1,
        TessEvalBit    = 1 << 2,
        ComputeBit     = 1 << 3,
        PixelBit       = 1 << 4,

        AllStages = 0xff
    };
}

using T = std::underlying_type_t <gfx::ShaderStageFlags>;

inline gfx::ShaderStageFlags operator | (gfx::ShaderStageFlags lhs, gfx::ShaderStageFlags rhs)

{
    return (gfx::ShaderStageFlags)(static_cast<T>(lhs) | static_cast<T>(rhs));
}

inline gfx::ShaderStageFlags& operator |= (gfx::ShaderStageFlags& lhs, gfx::ShaderStageFlags rhs)
{
    lhs = (gfx::ShaderStageFlags)(static_cast<T>(lhs) | static_cast<T>(rhs));
    return lhs;
}
