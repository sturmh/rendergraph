#pragma once

namespace gfx {
    enum BufferAccessFlags {
        GpuReadBit  = 1 << 0,
        GpuWriteBit = 1 << 1,
        CpuReadBit  = 1 << 2,
        CpuWriteBit = 1 << 3,

        GpuReadCpuWriteBits = GpuReadBit | CpuWriteBit,
    };
}


