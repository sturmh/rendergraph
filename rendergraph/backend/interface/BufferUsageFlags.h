#pragma once

namespace gfx {
    enum BufferUsageFlags {
        None              = 0,
        VertexBufferBit   = 1 << 0,
        IndexBufferBit    = 1 << 1,
        ConstantBufferBit = 1 << 2,
    };
}
