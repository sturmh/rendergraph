//
//  RenderGraphResource.hpp
//  rendergraph
//
//  Created by Eugene Sturm on 4/12/18.
//  Copyright © 2018 Eugene Sturm. All rights reserved.
//

#ifndef RenderGraphResource_hpp
#define RenderGraphResource_hpp

#include "AttachmentDesc.h"
#include <set>
#include <string>

class RenderPass;

enum class RenderGraphResourceType : uint8_t
{
    Texture
};

class RenderGraphResource
{
public:
    RenderGraphResource(RenderGraphResourceType type, const AttachmentDesc& desc, const std::string& name = "");
    
    void addWritePass(const RenderPass* pass);
    void removeWritePass(const RenderPass* pass);
    void addReadPass(const RenderPass* pass);
    void removeReadPass(const RenderPass* pass);
    
    const std::set<const RenderPass*>& readPasses() const;
    const std::set<const RenderPass*>& writePasses() const;
    
    RenderGraphResourceType type() const;
    const std::string& name() const;
    const AttachmentDesc& desc() const;
private:
    RenderGraphResourceType _type;
    AttachmentDesc _desc;
    std::string _name;
    std::set<const RenderPass*> _readPasses;
    std::set<const RenderPass*> _writePasses;
};



#endif /* RenderGraphResource_hpp */
